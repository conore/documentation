# Infrastructure as code - Conore

<img src="../.gitlab/images/infrastructure-as-code.png" />

## Prérequis

- Avoir installé `Vagrant` sur la machine host
- Avoir installé le plugin de `DigitalOcean` pour `Vagrant`
    ```bash  
        vagrant plugin install vagrant-digitalocean
    ```
- Avoir installé `Ansible` sur sa machine hôte
    ```bash
        sudo pip install ansible
    ```
- Avoir un compte `DigitalOcean`

## Configuration

Créez un fichier `secret.yml` avec votre token de DigitalOcean.

Recupérer le token : 
- Allez sur le dashboard de `DigitalOcean`
- Allez dans `Manage`>`API`
- Cliquez sur `Generate New Token`

```yml
    token: 'YOUR_TOKEN'
```

## Installation du cluster

Nous allons maintenant utiliser Vagrant pour créer nos VM et automatiquement déployer notre configuration avec nos fichiers ansible.

Nous avons donc juste besoin d'executer la commande suivante :

```bash
    vagrant up
```