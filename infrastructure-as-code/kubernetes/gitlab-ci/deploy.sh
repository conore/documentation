#!/bin/bash
# . ./deploy_function.sh
/kubernetes/gitlab-ci/deploy_function.sh

check_deployment=$(kubectl get deployment $1 --namespace=$2 2>&1 | grep NAME)
update_deployment="kubectl rollout restart deployment $1 --namespace=$2"
start_deployment="kubectl apply -f /kubernetes/deployments/$2/$1.yml"

if [ -z "$check_deployment" ]
then
    eval "$start_deployment"
    if [ "$1" = "api" ] && [ "$2" = "develop" ]
    then
      pods_status api develop
      deploy_api api develop
    elif [ "$1" = "api" ] && [ "$2" = "production" ]
    then
      pods_status api production
      deploy_api_prod api production
    fi
    echo "Started !"
else
    eval "$update_deployment"
    echo "Updated !"
fi

exit