#!/bin/bash

pods_status(){
  while [[ $(kubectl get pods -l app=$1 --namespace=$2 -o 'jsonpath={..status.conditions[?(@.type=="Ready")].status}') != "True" ]]; do echo "waiting for pod" && sleep 1; done
}

deploy_api(){
  pods=$(kubectl get pods --namespace=$2 | grep $1 | cut -d ' ' -f 1)
  for pod in $pods
  do
    kubectl exec -it $pod --namespace=$2 -- php bin/console d:d:c
    kubectl exec -it $pod --namespace=$2 -- php bin/console d:s:u -f
    kubectl exec -it $pod --namespace=$2 -- php bin/console d:f:l -n
  done
}

deploy_api_prod(){
  pods=$(kubectl get pods --namespace=$2 | grep $1 | cut -d ' ' -f 1)
  for pod in $pods
  do
    kubectl exec -it $pod --namespace=$2 -- php bin/console d:s:u -f
  done
}