# Kubernetes- Conore

<img src="../../.gitlab/images/kubernetes-conore.png" />

## Namespace develop

Deployments :

- dashboard
  - 1 replicas
- api
  - 1 replicas
- db
  - 1 replicas
  - 1 Persistant Claim (Persistance des données)

Ingress / Nginx 
- https://dev-app.conore.com
- https://dev-api.conore.com

## Namespace production

Deployments :

- dashboard
  - 3 replicas
- api
  - 3 replicas
- db
  - 3 replicas
  - 1 Persistant Claim (Persistance des données)
- landing
  - 1 replicas

Ingress / Nginx 
- https://app.conore.com
- https://api.conore.com
- https://conore.com